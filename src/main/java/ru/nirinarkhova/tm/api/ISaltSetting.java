package ru.nirinarkhova.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}

